const mongoose = require('mongoose')

mongoose.Promise = global.Promise;
function connect(){
    return new Promise(async (resolve, reject) => {
        await mongoose.connect('mongodb+srv://cidenet:1006361228@cluster0.fiq0o.mongodb.net/cidenet?retryWrites=true&w=majority', { 
            useNewUrlParser: true,
            useUnifiedTopology: true  
        }).then(()=> {
            resolve("db is connected")
            return true;
        }).catch(()=> {
            reject("error in connection")
        })
    })
}

module.exports = {
    connect: connect
}