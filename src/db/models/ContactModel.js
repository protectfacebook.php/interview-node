const mongoose = require('mongoose')
const paginate = require('mongoose-paginate-v2')
const schema = mongoose.Schema;
const contactSchema = new schema({
    name: {
        type: String,
        min: 2,
        max: 25 
    },
    lastName: {
        type: String,
        min: 2,
        max: 25 
    },
    phone: {
        type: String,
        min: 2,
        max: 25 
    },
    email: {
        type: String, 
        min: 6,
        max: 255
    },
    address: {
        type: String,
        min: 6,
        max: 255
    },
    user: {
        type: String,
        required: true
    }
})

contactSchema.plugin(paginate)
const contactModel = mongoose.model('Contact', contactSchema)
module.exports = contactModel;
