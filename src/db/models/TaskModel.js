const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')

const schema =  mongoose.Schema;
const taskScheme = new schema({
    name: {
        required: true,
        type: String,
        min: 5,
        max: 100
    },
    description: {
        required: false,
        type: String,
    },
    status: {
        required: true,
        type: Boolean
    }
})

taskScheme.plugin(mongoosePaginate)
const taskModel = mongoose.model('Tasks', taskScheme)
module.exports = taskModel;