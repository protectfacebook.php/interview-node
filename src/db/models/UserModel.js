const mongoose = require('mongoose');
const schema = mongoose.Schema;
const paginate = require('mongoose-paginate-v2')
const userSchema = new schema({
    name: {
        required: true,
        type: String,
        min: 3, 
        max: 20
    }, 
    email: {
        type: String,
        unique: true,
        required: true,
        min: 6,
        max: 255
    },
    password: {
        type: String,
        required: true,
        min: 6, 
        max: 255
    },
    creationDate: {
        type: Date,
        default: Date.now()
    },
    resetCode: {
        type: String,
        default: '0000000'
    }
})
userSchema.plugin(paginate)
const userModel  = mongoose.model('User', userSchema)
module.exports = userModel;