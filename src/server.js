const bodyParser =  require('body-parser')
const db = require('./db/db')
const authRoutes = require('./auth/auth')
const authValidate =require('./middlewares/auth-jwt-validate')
const userRoutes = require('./routes/UserRoutes')
const tasksRoutes = require('./routes/TaskRoutes')
const contactRoutes = require('./routes/ContactRoutes')
const cors = require('cors');
const corsConfig  = {
    origin: '*',
}
require('dotenv').config()
class Server{
    port = process.env.port || 4000; 
    
    serverConfig(servidor) {
        servidor.use(bodyParser.json())
        servidor.use(bodyParser.urlencoded({extended: true}))
        servidor.use(cors(corsConfig))
        servidor.use('/api/auth', authRoutes);
        servidor.use('/api/user', authValidate,  userRoutes)
        servidor.use('/api/contact',  contactRoutes)
        servidor.use('/api/tasks', tasksRoutes)
    }

    startDb(){
        db.connect()
        .then(data => {
            console.log(data)
        }).catch(error => console.log(error))
    }

    connect = async(servidor) => {
        try {
            await servidor.listen(this.port, ()=> {
                console.log("server running at port 4000")
            })
            this.serverConfig(servidor)
            this.startDb();
        } catch (error) {
            console.log(error)
        }
    }
    
}


module.exports = Server;
