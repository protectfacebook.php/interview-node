const jwt  = require('jsonwebtoken');
function verifyToken(req, res, next ) {
    const token  =  req.header("Authorization")
    if(!token){
        //res.sendFile('../views/login.html')
        return res.status(401).json({ error: 'Access denied' })
    }
    try {
        const verified = jwt.verify(token, process.env.TOKEN_SECRET || '1006361228')
        req.user = verified
        next() 
    } catch (error) {
        res.status(400).json({error: 'Invalid token'})
    }
}

module.exports = verifyToken;