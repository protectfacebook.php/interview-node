const routes = require('express').Router();
const  Controller  = require('../controllers/UserController') 
const userController = new Controller();
const path = require('path')



routes.post('/register', (req, res) => {
    userController.register(req)
    .then(data => res.status(200).send(data))
    .catch(error  => res.status(error[0]).send(error[1]))
})
routes.post('/login', (req, res )=> {
    userController.login(req)
    .then(token  => {
        res.header('Authorization', token)
            .json({
                error: null
            })
    })
    .catch(error  => res.status(error[0]).send(error[1]))
})

routes.post('/forgot', (req, res) => {
    userController.forgotPassword(req)
    .then(data => res.send(data))
    .catch(error => res.status(error[0]).send(error[1]))
})


routes.post('/resetPassword', (req, res ) => {
    userController.resetPassword(req)
    .then(data => res.status(200).send(data))
    .catch(error => res.status([error[0]]).send(error[1]))
} )
module.exports  = routes;
