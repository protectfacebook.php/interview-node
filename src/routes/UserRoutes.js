const routes = require('express').Router()
const controller  =  require('../controllers/UserController')
const UserController = new controller();

routes.post('/profile', (req, res)=> {
    UserController.profile(req)
    .then(data => res.status(200).send(data))
    .catch(error => res.status(error[0]).send(error[1]))
} )
module.exports = routes; 