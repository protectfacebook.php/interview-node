const routes = require('express').Router()
const controller = require('../controllers/TaskController')
const taskController = new controller()

routes.post('/save', (req, res) => {
    taskController.save(req).then(response => {
        res.status(200).send(response)
    })
    .catch(error => {
        res.status(error[0]).send(error[1])
    })
} )


routes.get('/get-all', (req, res) => {
    taskController.getAll().then(response => {
        res.status(200).send(response)
    })
    .catch(error => {
        res.status(error[0]).send(error[1])
    })
})

routes.get('/get-task-by-id', (req, res) => {
    taskController.getTaskById(req)
    .then(response  =>{
        res.status(200).send(response)
    })
    .catch(error => {
        res.status(error[0]).send(error[1])
    })
})

routes.put('/update-by-id', (req, res) => {
    taskController.updateTask(req)
    .then(response => res.status(200).send(response))
    .catch(error => res.status(error[0]).send(error[1]))
})

module.exports = routes