const  routes = require('express').Router()
const controller = require('../controllers/ContactController')
const contactController = new controller()

routes.post('/create', (req, res )=> {
    contactController.save(req)
    .then(data  => {
        res.status(200).send(data)
    })
    .catch(error=> {
        res.status(error[0]).send(error[1])
    })
})
routes.get('/searchAllByUser', (req, res )=> {
    contactController.searchAllByUser(req)
    .then(data  => {
        res.status(200).send(data)
    })
    .catch(error=> {
        res.status(error[0]).send(error[1])
    })
})

routes.get('/searchByUser', (req, res )=> {
    contactController.searchByUser(req)
    .then(data  => {
        res.status(200).send(data)
    })
    .catch(error=> {
        res.status(error[0]).send(error[1])
    })
})

routes.put('/update', (req, res) => {
    contactController.updateOne(req)
    .then(data => {
        res.status(200).send(data)
    })
    .catch(error => {
        res.status(error[0]).send(error[1])
    })
})
routes.delete('/delete', (req, res )=> {
    contactController.deleteOne(req)
    .then(data => {
        res.status(200).send(data)
    })
    .catch(error => {
        res.status(error[0]).send(error[1])
    })
})

module.exports = routes;