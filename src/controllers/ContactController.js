const Joi = require('@hapi/joi')
const Contact  = require('../db/models/ContactModel')


 class ContactController{
     save(req){
         return new Promise(async(resolve, reject) => {
             const contactValidator = Joi.object({
                name: Joi.string().min(2).max(25).required(),
                lastName: Joi.string().min(2).max(25).required(),
                email: Joi.string().min(6).max(255).required().email(),
                phone: Joi.string().min(6).max(25).required(),
                address: Joi.string().min(6).max(25).required(),
                user: Joi.string()
             })
             const {error } = contactValidator.validate(req.body)
             if(error){
                reject([400, error])
                return false;
            }
            const contact = new Contact(req.body)
            try {
                const contactCreated = await contact.save();
                resolve(contactCreated)
            } catch (error) {
                reject([404, error])
            }
         })
     }

     searchAllByUser(req){
         return new Promise(async(resolve, reject) => {
             if(!req.query.id){
                 reject([400, {error:  'Id not found'}])
             }
             const page  = req.query.page || 1;  
             await Contact.paginate({user: req.query.id}, {limit: 10, page: page})
             .then(data => {
                 resolve(data)
             })
             .catch(error => reject([400, error]))
         })
     }
     searchByUser(req){
         return new Promise(async(resolve, reject) => {
            if(!req.query.id){
                reject([400, {error:  'Id not found'}])
            }
            await Contact.find({user: req.query.id, name: req.query.name}, {user: 0})
            .then(data => resolve(data))
            .catch(error => reject([400, error]))
         })
     }

     updateOne(req){
         return new Promise(async(resolve, reject) => {
             if(!req.body){
                 reject([400, { error : 'Object not found'}])
             } 
             await Contact.updateOne({_id: req.body._id}, {$set: req.body})
             .then(data => resolve(data))
             .catch(error => reject([400, error]))
         })
     }
    
     deleteOne(req){
         return new Promise(async(resolve, reject) => {
            if(!req.query.id){
                reject([400, {error:  'Id not found'}])
            }
            try {
                await Contact.deleteOne({_id: req.query.id})
                resolve('Contact deleted')
            } catch (error) {
                reject([400, error])
            }
         })
     }

   
 }

 module.exports = ContactController;
