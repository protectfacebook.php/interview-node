const User = require('../db/models/UserModel')
const bcrypt = require('bcrypt')
const Joi = require('@hapi/joi')
const jwt = require('jsonwebtoken')
const mailService = require('../service/MailService')


class UserController{

    register(req) {
        return new Promise(async(resolve, reject ) => {
            const schemaRegister = Joi.object({
                name: Joi.string().min(3).max(20).required(),
                email: Joi.string().min(6).max(255).required().email(),
                password: Joi.string().min(6).max(255).required()
            })
            const {error} = schemaRegister.validate(req.body)
            if(error){
                reject([400, error])
                return false;
            }
            

            const existEmail = await User.findOne({email  : req.body.email})
            if(existEmail){
                reject([400, {error: "email already exist"}])
                return false;
            }
          


            const salt  = await bcrypt.genSalt(12)
            const hash = await  bcrypt.hash(req.body.password, salt)
            const user = new User({
                name: req.body.name,
                email: req.body.email,
                password: hash
            })
            
            try {
                const userCreated = await user.save();
                resolve(userCreated)
            } catch (error) {
                reject([400, error])
            }
        
        })
    }

    login(req){
        return new Promise(async(resolve, reject) => {
            const schemaLogin  = Joi.object({
                email: Joi.string().min(6).max(255).required().email(),
                password: Joi.string().min(6).max(255).required()
            })
            const {error} = schemaLogin.validate(req.body)
            if (error) {
                reject([400, error])
                return false;
            }
            const user = await User.findOne({email: req.body.email});
            if(!user){
                reject([404, {error: "No user was found with that email"}])
            }
            const password = req.body.password;
            
            const validPassword = await bcrypt.compare(password, user.password);
            if (!validPassword) {
                reject([401, {error: "Incorrect User or Password"}])
            }
            const token = jwt.sign({
                name : user.name,
                email : user.email, 
                id: user._id
            }, process.env.TOKEN_SECRET || '1006361228', {expiresIn: 3600})
            resolve(token)
        })

    }
    profile(req){
        const id = req.body.id;
        return new Promise(async(resolve, reject)=> {
            if(!id){
                reject("id not found")
            }
            try {
                const user = await User.findOne({"_id": id}, {_id: 0, name: 1, email: 1, password: 0})
                if(!user) {
                    reject([404, "Object not found"])
                }
                resolve(user)
            } catch (error) {
                reject([400, "Internal server error"])
            }
            
        })
    }

    forgotPassword(req) {
        return new Promise(async (resolve, reject) => {
            const email = req.body.email; 
            const userFound  = await User.findOne({email: email}).then(data => data)
            if(!userFound){
                reject([404, {error: 'No user registered with this email'}])
            }
            const token = jwt.sign({email: userFound.email, name: userFound.name}, '1006361228', {expiresIn: 1200} )
            const data = {
                from: 'marlondevjs@gmail.com',
                to: userFound.email,
                subject: "Recovery account link", 
                html: `
                        <h2>Da click en el siguiente link para recuperar tu contraseña</h2>
                        <p>http://localhost:4000/api/auth/reset-password?id=${token}</p>
                    `
            }
            userFound.resetCode = token;
            
            try {
                await User.updateOne({_id: userFound._id}, { $set: userFound}, (err, success) => {
                    if(err){
                        reject([400, err])
                    }
                    mailService.senEmail(data).then(data => resolve(data))
                    .catch(error => {
                        console.log(error)
                        reject([400, error ])})
                })
            } catch (error) {
                reject([400, error])
            }
        })
    }
    resetPassword(req){
        return new Promise(async(resolve, reject) => {
            let token = req.query.id; 
           const user = await User.findOne({resetCode: token}).then(data => data)
           if(!user){
               reject([404, {error: 'usuario no encontrado'}])
           }
           if(token !== user.resetCode){ 
               reject([400, {error: 'el link no es valido'}])
           }
           jwt.verify(token, '1006361228', async (err, decodedToken)=> {
               if(err){
                   reject([401,  err])
               }
               const salt  =  bcrypt.genSaltSync(12)
                const hash =   bcrypt.hashSync(req.body.password, salt)
               const {email, name} = jwt.decode(token);
               
              try {
                const userUpdated  = await User.updateOne({resetCode: token}, {$set: {
                    name: name,
                    email: email,
                     password: hash 
                }});
                resolve(userUpdated)
              } catch (error) {
                  reject([400, error])
              }
           })
        })
    }
}


module.exports = UserController;
