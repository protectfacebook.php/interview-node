const joi = require("@hapi/joi");
const taskModel = require("../db/models/TaskModel");

class TaskController {
  save(req) {
    return new Promise(async (resolve, reject) => {
      const registerScheme = joi.object({
        name: joi.string().min(5).max(100).required(),
        description: joi.string(),
        status: joi.boolean().required(),
      });
      const { error } = registerScheme.validate(req.body);
      if (error) {
        return reject([401, error]);
      }
      const task = new taskModel({
        name: req.body.name,
        description: req.body.description,
        status: req.body.status,
      });

      try {
        const response = await task.save();
        resolve(response);
      } catch (error) {
        reject([500, {error}]);
      }
    });
  }

  getAll() {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await taskModel.find();
        resolve(response);
      } catch (error) {
        reject([400, {error}]);
      }
    });
  }

  getTaskById(req) {
    return new Promise(async (resolve, reject) => {
      const id = req.query.taskId;
      if (id === undefined) {
          return reject([400, {error: 'task id not found'}])
      }
      try {
          const taskFound = await taskModel.findById(id)
          return resolve(taskFound)
      } catch (error) {
          return reject([404, {error}])
      }

    });
  }


  updateTask(req){
    return new Promise(async(resolve, reject)=> {
      const id = req.body.id
      if (id === undefined) {
        return reject([400, {error: "task id not found"}])
      }
      try {
        const taksUpdated = await taskModel.updateOne({_id: id}, {$set: req.body})
        return resolve(taksUpdated)
      } catch (error) {
        return reject([400, {error}])
      }
    })
  }
}

module.exports = TaskController;
