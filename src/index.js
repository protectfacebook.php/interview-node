const servidor = require('./server')
const express = require('express');
const morgan = require('morgan')
const serverObject = new servidor();
const app = express();


function main(){
    serverObject.connect(app)
    app.use(morgan('dev'))
}

main();